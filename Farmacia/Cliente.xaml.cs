﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Farmacia
{
    /// <summary>
    /// Lógica de interacción para Cliente.xaml
    /// </summary>
    public partial class Cliente : Window
    {
        RepositorioDeClientes repositorio;
        bool esNuevo;

        public Cliente()
        {
            InitializeComponent();
        }
        private void HabilitarCajas(bool habilitadas)
        {



            txbNombre.Clear();
            txbTelefono.Clear();
            txbCorreo.Clear();
            txbDireccion.Clear();
            txbRFC.Clear();
            txbNombre.IsEnabled = habilitadas;
            txbTelefono.IsEnabled = habilitadas;
            txbCorreo.IsEnabled = habilitadas;
            txbDireccion.IsEnabled = habilitadas;
            txbRFC.IsEnabled = habilitadas;

        }

        private void HabilitarBotones(bool habilitados)
        {

            btnNuevo.IsEnabled = habilitados;
            btnEditar.IsEnabled = habilitados;
            btnEliminar.IsEnabled = habilitados;
            btnGuardar.IsEnabled = habilitados;

        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(true);
            HabilitarBotones(false);
            esNuevo = true;
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(false);
            HabilitarBotones(true);
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txbNombre.Text) || string.IsNullOrEmpty(txbDireccion.Text) || string.IsNullOrEmpty(txbTelefono.Text))
            {
                MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            if (esNuevo)
            {

                Clientes c = new Clientes()
                {

                    Nombre = txbNombre.Text,
                    RFC = txbRFC.Text,
                    Correo = txbCorreo.Text,
                    Telefono = txbTelefono.Text,
                    Direccion = txbDireccion.Text,
                };
                if (repositorio.AgregarAmigo(c))
                {
                    MessageBox.Show("Guardado con Éxito", "cliente", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTabla();
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                }
                else
                {
                    MessageBox.Show("Error al guardar cliente", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Clientes original = dtgTabla.SelectedItem as Clientes;
                Clientes c = new Clientes();

                c.Nombre = txbNombre.Text;
                c.RFC = txbRFC.Text;
                c.Correo = txbCorreo.Text;
                c.Telefono = txbTelefono.Text;
                c.Direccion = txbDireccion.Text;

                if (repositorio.ModificarCliente(original, c))
                {
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                    ActualizarTabla();
                    MessageBox.Show("Su cliente a sido actualizado", "cliente", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Error al guardar a tu cliente", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        private void ActualizarTabla()
        {
            dtgTabla.ItemsSource = null;
            dtgTabla.ItemsSource = repositorio.LeerCliente();
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (repositorio.LeerCliente().Count == 0)
            {
                MessageBox.Show("Nadien quiero ser tu cliente..", "No tienes clientes", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgTabla.SelectedItem != null)
                {
                    Cliente c = dtgTabla.SelectedItem as Cliente;
                    if (MessageBox.Show("Realmente deseas eliminar a clientes" + c.dtgCliente + "?", "Eliminar????", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        if (repositorio.EliminarCliente(c))
                        {
                            MessageBox.Show("Tu cliente ha sido removido", "cliente", MessageBoxButton.OK, MessageBoxImage.Information);
                            ActualizarTabla();
                        }
                        else
                        {
                            MessageBox.Show("Error al eliminar a tu cliente, ", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("¿A Quien???", "cliente", MessageBoxButton.OK, MessageBoxImage.Question);
                }
            }
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (repositorio.LeerCliente().Count == 0)
            {
                MessageBox.Show("Nadie  quiere ser tu cliente...", "No tienes Cliente", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgTabla.SelectedItem != null)
                {
                    Clientes c = dtgTabla.SelectedItem as Clientes;
                    HabilitarCajas(true);

                    txbNombre.Text = c.Nombre;
                    txbRFC.Text = c.RFC;
                    txbCorreo.Text = c.Correo;
                    txbTelefono.Text = c.Telefono;
                    txbDireccion.Text = c.Direccion;


                    HabilitarBotones(false);
                    esNuevo = false;
                }
                else
                {
                    MessageBox.Show("¿A Quien???", "cliente", MessageBoxButton.OK, MessageBoxImage.Question);
                }


            }
        }

        private void btnRegresar_Click(object sender, RoutedEventArgs e)
        {


            this.Hide();
            MainWindow principal = new MainWindow();
            principal.Owner = this;
            principal.Show();
        }
    }
}
