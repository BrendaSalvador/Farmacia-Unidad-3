﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Farmacia
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnProductos_Click(object sender, RoutedEventArgs e)
        {
            ProductoNuevo productoNuevo = new ProductoNuevo();
            productoNuevo.Show();

        }

        private void btnEmpleados_Click(object sender, RoutedEventArgs e)
        {
            Empleado empleado = new Empleado();
            empleado.Show();
        }

        private void btnVentas_Click(object sender, RoutedEventArgs e)
        {
            Venta venta = new Venta();
            venta.Show();
        }

        private void btnClientes_Click(object sender, RoutedEventArgs e)
        {
            Empleado cliente = new Empleado();
            cliente.Show();
        }
    }
}
