﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Farmacia
{
    /// <summary>
    /// Lógica de interacción para ProductoNuevo.xaml
    /// </summary>
    public partial class ProductoNuevo : Window
    {
        List<ProductoNuevo> Producto;
        bool esNuevo;
        public ProductoNuevo()
        {
            InitializeComponent();
            Producto = new List<ProductoNuevo>();
            HabilitarCajas(true);
            HabilitarBotones(true);
            ActualizarTabla();

        }

        private void HabilitarCajas(bool habilitadas)
        {
            txbNombre.Clear();
            txbPrecioCompra.Clear();
            txbPresentacion.Clear();
            txbDescripcion.Clear();
            txbPrecioVenta.Clear();
            txbCategoria.Clear();
            txbPrecioCompra.IsEnabled = habilitadas;
            txbPresentacion.IsEnabled = habilitadas;
            txbDescripcion.IsEnabled = habilitadas;
            txbNombre.IsEnabled = habilitadas;
            txbCategoria.IsEnabled = habilitadas;
        }


        private void HabilitarBotones(bool habilitados)
        {
            btnNuevo.IsEnabled = habilitados;
            btnEditar.IsEnabled = habilitados;
            btnEliminar.IsEnabled = habilitados;
            btnGuardar.IsEnabled = !habilitados;

        }




        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txbNombre.Text) || string.IsNullOrEmpty(txbNombre.Text) || string.IsNullOrEmpty(txbPrecioCompra.Text))
            {
                MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            if (esNuevo)
            {

                ProductosNuevos p = new ProductosNuevos()
                {
                    PrecioCompra = txbPrecioCompra.Text,
                    Categoria = txbCategoria.Text,
                    Presentacion = txbPresentacion.Text,
                    Nombre = txbNombre.Text,
                    Descripcion = txbDescripcion.Text,
                    PrecioVenta = txbPrecioVenta.Text,
                };
                Producto.Add(null);
                MessageBox.Show("Guardado con Éxito", "Producto", MessageBoxButton.OK, MessageBoxImage.Information);
                ActualizarTabla();
                HabilitarBotones(true);
                HabilitarCajas(false);
            }
            else
            {
                ProductosNuevos p = dtgTabla.SelectedItem as ProductosNuevos;
                p.PrecioCompra = txbPrecioCompra.Text;
                p.Categoria = txbCategoria.Text;
                p.Presentacion = txbPresentacion.Text;
                p.Nombre = txbNombre.Text;
                p.Descripcion = txbDescripcion.Text;
                p.PrecioVenta = txbPrecioVenta.Text;
                HabilitarBotones(true);
                HabilitarCajas(false);
                ActualizarTabla();
                MessageBox.Show("Tus productos han sido eliminados", "Producto", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private void ActualizarTabla()
        {
            dtgTabla.ItemsSource = null;
            dtgTabla.ItemsSource = Producto;
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (Producto.Count == 0)
            {
                MessageBox.Show("Nada", "no tienes productos", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgTabla.SelectedItem != null)
                {
                    ProductoNuevo p = dtgTabla.SelectedItem as ProductoNuevo;
                    if (MessageBox.Show("Realmente deseas eliminar a " + p.Producto + "Producto", "Eliminar????", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        Producto.Remove(p);
                        MessageBox.Show("Tus productos han sido eliminados", "Producto", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTabla();
                    }
                }
                else
                {
                    MessageBox.Show("¿A Quien???", "Producto", MessageBoxButton.OK, MessageBoxImage.Question);
                }
            }
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (Producto.Count == 0)
            {
                MessageBox.Show("Nada", "no tienes productos", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgTabla.SelectedItem != null)
                {
                    ProductosNuevos p = dtgTabla.SelectedItem as ProductosNuevos;
                    HabilitarCajas(true);
                    txbPrecioCompra.Text = p.PrecioCompra;
                    txbCategoria.Text = p.Categoria;
                    txbPresentacion.Text = p.Presentacion;
                    txbNombre.Text = p.Nombre;
                    txbDescripcion.Text = p.Descripcion;
                    txbPrecioVenta.Text = p.PrecioVenta;
                    HabilitarBotones(false);
                    esNuevo = false;
                }
                else
                {
                    MessageBox.Show("¿A Quien???", "Producto", MessageBoxButton.OK, MessageBoxImage.Question);
                }
            }
        }

    
        private void btnNuevo_Click_1(object sender, RoutedEventArgs e)
        {
           
                HabilitarCajas(true);
                HabilitarBotones(false);
                esNuevo = true;
           

            }

        private void btnRegresar_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            MainWindow principal = new MainWindow();
            principal.Owner = this;
            principal.Show();
        }
    }
    }

       
    
